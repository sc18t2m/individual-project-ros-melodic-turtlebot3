import actor_path_gen
import numpy as np

''' This script is to generate file with fully working
    actor model for Gazebo world.

    In order to generate moving actors, path waypoints
    need to be throught path_data variable. For more
    instructions read actor_path_gen.py script.

    author: Tomas Martinek
    email:  tomas-martinek@email.cz                 '''


# *** USER INPUT ***
actor_name = "Standing blue"
walking = False # True for walknig, False for standing actor
actor_color = "blue" # actor T-shirt and skin color
speed = 0.5 # actor speed m/s (reasonable speed is 0.5 < speed < 0.75)
mirrored = 1 # is path mirrored? (1=no, 2=yes)

# For standing actors only
location = [4,-5.3,-1.57] # x,y,direction

# For walking actors only (instructions in actor_path_gen.py)
actor_path = np.matrix([
[-21, -18, 1, 8],
[-21, -15, 0, 0],
[-23, -15, 2, 3],
[-20, -15, 0, 0],
[-18, -13, 0, 0],
[-19, -2, 2, 3],
[-17.3, -2, 0, 0],
[-17.3, -7, 0, 0],
[-18, -8, 0, 0],
[-18, -15, 0, 0],
[1, -15, 0, 0],
[1.5, -14, 1.5, 3],
[3, -12.5, 0, 0],
[-4, -11, 0, 0],
[-4.5, -9, 0, 0],
[-2, -9, 3, 3],
[-2, -7.7, 1, 3],
[-4, -7.7, 0, 0],
[-4, 1, 0, 0],
[-2, 0.7, 3, 3],
[-2, 1.8, 0, 0],
[-13, 3, 0, 0],
[-12, 5, 0.1, 3],
[-12, -2, 0.1, 3],
[-12, 0, 0, 0],
[-5, 2, 0, 0],
[-3, 3.7, 3.5, 3],
[-4, 9, 0, 0],
[-2, 10, 3.5, 3],
[-5, 13, 0, 0],
[-8, 13, 0, 0],
[-10, 10, 2, 3],
[-10, 14, 0, 0],
[5, 14, 0, 0],
[5, 12, 3.5, 3],
[7, 14, 0, 0],
[9, 12, 0, 0],
[4, 7, 0, 0],
[4.5, 5.5, 3.5, 3],
[1.5, 3, 0, 0],
[2, 0.5, 3, 3],
[9, 1, 0, 0],
[9, -2, 0, 0],
[7, -3, 3, 3],
[9, -3, 0, 0],
[13, -6, 0, 0],
[13, -7, 0, 0],
[12.5, -7.5, 1.5, 3],
[14.5, -8, 1.5, 3],
[21, -8, 0, 0],
[21, -18, 0, 0],
[-21, -18, 0, 0]
])


# ***** PARAMETERS *****
FILE_NAME = "actor.txt" # file path + name
PATH_FILE = "path.txt" # output file for actor_path_gen.py
LOG_FILE = "log.txt" # logs actor_path
SKIN_PATH = "file://../models/actor_meshes/" # .dae files path
STANDING = "_stand.dae" # last part of file for standing actors
WALKING = "_walk.dae" # last part of file for walking actors


# *** FILE GENERATING ***
# Generate path - path to be edited in actor_path_gen.py
actor_path_gen.generate_path_file(actor_path, speed, mirrored)

# Create file
f = open(FILE_NAME, "w")
f.close()

# Read path file
f = open(PATH_FILE, "r")
path = f.read()
f.close()

# Open file for actor model
f = open(FILE_NAME, "a")

# Write comment on top
f.write("<!-- SCRIPTED: " + actor_name + ", color: " + actor_color +\
        ", walking: " + str(walking) + " -->\n")

# For standing actors
if (not walking):
    actor_skin_file = SKIN_PATH + actor_color + STANDING
    # Actor tag
    f.write("<actor name = \"" + actor_name + "\">\n")
    # Skin and Filename tag
    f.write("  <skin>\n    <filename>" + actor_skin_file + "</filename>\n  </skin>\n")
    # Script and Trajectory tag
    f.write("  <script>\n    <trajectory id=\"0\" type=\"stand\">\n")
    # Waypoints for standing actors
    f.write("      <waypoint>\n        <time>0</time>\n        <pose>" + str(location[0]) + " "\
            + str(location[1]) + " 0 0 0 " + str(location[2]) + "</pose>\n      </waypoint>\n" +\
            "      <waypoint>\n        <time>60</time>\n        <pose>" + str(location[0]) + " "\
            + str(location[1]) + " 0 0 0 " + str(location[2]) + "</pose>\n      </waypoint>\n")
    # Closing tags
    f.write("    </trajectory>\n  </script>\n</actor>")

# For walking actors
if (walking):
    actor_skin_file = SKIN_PATH + actor_color + WALKING
    # Actor tag
    f.write("<actor name = \"" + actor_name + "\">\n")
    # Skin and Filename tag
    f.write("  <skin>\n    <filename>" + actor_skin_file + "</filename>\n  </skin>\n")
    # Animation tag
    f.write("  <animation name=\"walking\">\n    <filename>walk.dae</filename>\n" + \
            "    <interpolate_x>true</interpolate_x>\n  </animation>")
    # Script and Trajectory tag
    f.write("  <script>\n    <trajectory id=\"0\" type=\"walking\">\n")
    # Waypoints from path.txt file
    f.write( "      " + path.replace("\n", "\n      ") )
    # Closing tags
    f.write("</trajectory>\n  </script>\n</actor>")

# Close the actor.txt file
f.close()

# Add 4 spaces before every line is it matches coding style of world files
spacing = "    " # 4 spaces
# Read the file
f = open(FILE_NAME, "r")
f_as_txt = f.read()
f.close()
# Re-write the file with desired spacing
f = open(FILE_NAME, "w")
f.write(spacing + f_as_txt.replace("\n", "\n" + spacing))
f.close()

# Logging - in case some path requires editing later
if (walking):
    f = open(LOG_FILE, "a")
    f.write(actor_name + "\n" + str(actor_path) + "\n\n")
    f.close
    # Seuccess message
    print("Success - log.txt updated!")

# Success message
print("Success - actor.txt generated!")