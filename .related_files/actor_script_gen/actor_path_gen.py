import numpy as np
import math as m

''' This script is to generate file with path waypoints
    for human actors for Gazebo simulations.

    author: Tomas Martinek
    email:  tomas-martinek@email.cz                 '''

# Generate path file with waypoints function
def generate_path_file(path_data, speed, mirrored):


    # ***** SCRIPT GUIDE *****
    # Array of waypoints with direction and time information
    ''' data = np.matrix([[x, y, direction, time]])

        Time != 0 -> standing in place for period
        of time (in seconds) facing direction.

        Direction: 1 = 12 o'clock, 2 = 9 o'clock,
                   3 =  6 o'clock, 0 = 3 o'clock

        Example input data
        data = np.matrix([[1, 1, 3, 3],
                          [2, 3, 0, 0], 
                          [3, 4, 2, 3]])            '''


    # ***** PARAMETERS *****
    FILE_NAME = "path.txt" # file path + name
    ROTATION_T = 1 # 1 s/rotation
    SPEED = 0.75 # m/s
    MIRRORED = 1 # is path mirrored? (1=no, 2=yes)


    # ***** USER INPUT *****
    data = np.matrix([[-2, 4, 0.5, 3],
                      [-3, 1,   0, 0],
                      [-8, 1,   0, 0],
                      [-8, 1.5, 1, 2]])


    # ***** WAYPOINT CALCULATIONS *****
    # Create file
    f = open(FILE_NAME, "w")
    f.close()

    # Open file in append mode
    f = open(FILE_NAME, "a")

    # Check arguments
    if (path_data != np.matrix([])):
        data = path_data
    if (speed != 0):
        SPEED = speed
    if (mirrored != 0):
        MIRRORED = mirrored

    # Variables
    time = 0
    previous_direction = data[0,2] * m.pi/2

    # Initial waypoint
    f.write("<waypoint>\n  <time>" + str(time) + "</time>\n  <pose>" + str(data[0,0]) + \
            " " + str(data[0,1]) + " 0 0 0 " + str(data[0,2]*m.pi/2) + "</pose>\n</waypoint>\n")

    # Loop through waypoints in given order and backward order
    for loop in range(MIRRORED):
        # Loop through all waypoints
        for i in range(len(data)-1):
            # Check for standing
            if (data[i,3] != 0):
                # Rotate before standing
                if (previous_direction != data[i,2] * m.pi/2):
                    previous_direction = data[i,2] * m.pi/2
                    time += ROTATION_T
                    f.write("<waypoint>\n  <time>" + str(time) + "</time>\n  <pose>" + str(data[i,0]) + " "\
                            + str(data[i,1]) + " 0 0 0 " + str(data[i,2]*m.pi/2) + "</pose>\n</waypoint>\n")
                # Stand for given period of time
                time += data[i,3]
                f.write("<waypoint>\n  <time>" + str(time) + "</time>\n  <pose>" + str(data[i,0])\
                        + " " + str(data[i,1]) + " 0 0 0 " + str(data[i,2]*m.pi/2) + "</pose>\n</waypoint>\n")
            # Calculate direction (rotation angle) of the next waypoint
            direction = m.atan2(data[i+1,1]-data[i,1], data[i+1,0]-data[i,0])
            # Check current direction against desired direction
            if (direction != previous_direction):
                time += ROTATION_T
                f.write("<waypoint>\n  <time>" + str(time) + "</time>\n  <pose>" + str(data[i,0]) + " "\
                        + str(data[i,1]) + " 0 0 0 " + str(direction) + "</pose>\n</waypoint>\n")
            # Assign previous direction
            previous_direction = direction
            # Go to the next point
            distance = m.sqrt(abs(data[i,0]-data[i+1,0])**2 + abs(data[i,1]-data[i+1,1])**2)
            time += 1/SPEED * distance
            f.write("<waypoint>\n  <time>" + str(time) + "</time>\n  <pose>" + str(data[i+1,0]) + " " +\
                    str(data[i+1,1]) + " 0 0 0 " + str(direction) + "</pose>\n</waypoint>\n")

        # Rotate accordingly to the last waypoint
        li = len(data)-1 # last item id
        if (data[li,2]*m.pi/2 != previous_direction):
            time += ROTATION_T
            f.write("<waypoint>\n  <time>" + str(time) + "</time>\n  <pose>" + str(data[li,0]) + " " +\
                    str(data[li,1]) + " 0 0 0 " + str(data[li,2]*m.pi/2) + "</pose>\n</waypoint>\n")

        # Reverse waypoints
        data = np.flip(data, 0)

    # Close file
    f.close()

    # Success message
    print("Success - " + FILE_NAME + " generated!")


# Main call
if __name__ == '__main__':
    generate_path_file(np.matrix([]), 0, 0)