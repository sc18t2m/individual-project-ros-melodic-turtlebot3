#!/usr/bin/env python
'''
	Script subscribes to topic 'center_point' and obtain distance of this
    point from camera using sensor_msgs/Image from 'depth/image_raw' topic.

    Script publishes distance of the point as std_msgs/Float32 message to
    'pixel_depth' topic.

	author: Tomas Martinek, email: tomas-martinek@email.cz
'''

from __future__ import division
import rospy
import sys
import cv2
import numpy as np                                  # To convert numpy.uint8 to int
from sensor_msgs.msg import Image                   # To get distances of a point
from std_msgs.msg import Float32, Int16MultiArray   # Type for subscribed topics
from cv_bridge import CvBridge, CvBridgeError       # OpenCV class


class PixelDepth():
    def __init__(self):
        # Initialise publisher to publish depth of the pixel
        self.pub = rospy.Publisher('pixel_depth', Float32, queue_size=1)
        # Initialise CvBridge
        self.bridge = CvBridge()
        # Center point
        self.xcenter = 0
        self.ycenter = 0
        # Depth of the center pixel
        self.depth_pixel = 0
        # Subscribe to human detection topic
        rospy.Subscriber('center_point', Int16MultiArray, self.center)
        # Subscribe to depth camera
        rospy.Subscriber('depth/image_raw', Image, self.depth)

    def center(self, data):
        # Update center point obtained from 'center_point' topic
        self.xcenter = data.data[0]
        self.ycenter = data.data[1]

    def depth(self, data):
        # Return if xcenter and ycenter == 0
        if (self.xcenter == 0 and self.ycenter == 0):
            return
        # Convert sensor_msgs/Image into opencv image
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
        except:
            rospy.loginfo("Error OpenCV - while converting sensor_msgs/Image to OpenCV image!")
        # Get array of depth for every pixel in the image
        depth_array = np.array(cv_image, dtype=np.float32)
        # Check the pixel in the middle of the bounding box
        provisional_depth_pixel = depth_array[int(self.ycenter), int(self.xcenter)]
        # If the pixel depth is not reasonable number, use previous depth
        if (provisional_depth_pixel > 0.0 and provisional_depth_pixel < 5.0):
            self.depth_pixel = provisional_depth_pixel
        # Publish pixel depth into 'pixel_depth' topic as Float32
        self.pub.publish(self.depth_pixel)
        # Print pixel depth of the center pixel of the bounding box with person into console
        rospy.loginfo("Pixel depth: " + str(self.depth_pixel))
        # Reset center_point
        self.xcenter = 0
        self.ycenter = 0


# Main function
def main(args):
    # Initialise node
    rospy.init_node('pixel_depth')
    # Instantise PixelDepth
    pd = PixelDepth()
    # Esnsures that node continues running
    try:
        rospy.spin()
    except KeyboardInterrupt:
        rospy.loginfo("Rospy spin error - keyboard interrupt!")

			
# Check if the node is executing main
if __name__ == "__main__":
	main(sys.argv)