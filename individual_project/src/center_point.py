#!/usr/bin/env python
'''
    *** THIS SCRIPT IS REPLACED WITH 'actor_recognizer.py'! ***

    Script was tested and works well only if there is only one human actor
    around, 'actor_recognizer.py' is able to distinguish different actors.

	This script subscribes to topic 'darknet_ros/bounding_boxes' topic to obtain
    coordinates of rectangles (boxes) drawn by darknet_ros - YOLO (yolov2 - tiny)
    around humans in the camera view (topic '/camera/rgb/image_raw') in order to
    get information about center pixel of a human to navigate the robot.

    Script publishes (x, y) coordinates of the point as std_msgs/Int16MultiArray
	message to 'center_point' topic.

	author: Tomas Martinek, email: tomas-martinek@email.cz
'''

from __future__ import division
import rospy
import sys
from darknet_ros_msgs.msg import BoundingBoxes  # To get center point of a person
from std_msgs.msg import Int16MultiArray        # To publish center point coordinates


class CenterPoint():
    def __init__(self):
        # Initialise publisher to publish depth of the pixel
        self.pub = rospy.Publisher('center_point', Int16MultiArray, queue_size=1)
        # Subscribe to human detection topic
        rospy.Subscriber('darknet_ros/bounding_boxes', BoundingBoxes, self.center)

    def center(self, data):
        # Get bounding box location
        xmin = data.bounding_boxes[0].xmin
        xmax = data.bounding_boxes[0].xmax
        ymin = data.bounding_boxes[0].ymin
        ymax = data.bounding_boxes[0].ymax
        # Calculate bounding box center
        xcenter = (xmin + xmax) / 2
        ycenter = (ymin + ymax) / 2
        # Create Int16MultiArray containing center point coordinates
        center_point = Int16MultiArray(data=[xcenter, ycenter])
        # Publish the center_point into 'center_point' topic
        self.pub.publish(center_point)
        # Print center_point into console
        rospy.loginfo("Center point x, y = " + str(center_point.data[0]) + ", " + str(center_point.data[1]))


# Main function
def main(args):
    # Initialise node
    rospy.init_node('center_point')
    # Instantise CenterPoint
    cp = CenterPoint()
    # Esnsures that node continues running
    try:
        rospy.spin()
    except KeyboardInterrupt:
        rospy.loginfo("Rospy spin error - keyboard interrupt!")

			
# Check if the node is executing main
if __name__ == "__main__":
	main(sys.argv)