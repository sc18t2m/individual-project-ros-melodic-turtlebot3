#!/usr/bin/env python
'''
	Script copies appropriate map files into "~/catkin_ws" and
    publishes initial pose coordinates for navigation.

    Script publishes into topics:
	  * 'file_organizer' publishing initial pose coordinates

	author: Tomas Martinek, email: tomas-martinek@email.cz
'''

import rospy
import sys
import os		# For home directory path
from std_msgs.msg import Float32MultiArray		# For publishing initial coordinates

POSE_PATH = os.path.expanduser("~") + "/catkin_ws/src/turtlebot3/turtlebot3_navigation/maps/initial_pose/"
YAML_PATH = os.path.expanduser("~") + "/catkin_ws/src/turtlebot3/turtlebot3_navigation/maps/"
POSE_FILE = "pose_world"
YAML_FILE = "map.yaml"
MAP_FILE  = "world"


class FileOrganizer():
	def __init__(self):
		# Publish to cmd_wel topic - controls robot moevemnt
		self.pub = rospy.Publisher('file_organizer', Float32MultiArray, queue_size=1)
		# Change MAP_FILE path in YAML_FILE to load correct map
		self.edit_map_path()
		# Publish initial position coordinates from POSE_FILE
		self.publish_pose()

	def publish_pose(self):
		# Read pose file
		f = open(POSE_PATH + POSE_FILE, "r")
		pose_as_str = f.read()
		# Close file
		f.close()
		# Get pose coordinates
		cords_as_str = pose_as_str.split(';')
		x_cord = float(cords_as_str[0])
		y_cord = float(cords_as_str[1])
		w_cord = float(cords_as_str[2])
		# Create Float32MultiArray for publishing
		coordinates = Float32MultiArray(data = [x_cord, y_cord, w_cord])
		# Publish coordinates
		for i in range(10):
			self.pub.publish(coordinates)
			rate = rospy.Rate(10)
			rate.sleep()
		# Success message
		rospy.loginfo("Success - Coordinates published!")

	def edit_map_path(self):
		# Read YAML_FILE
		f = open(YAML_PATH + YAML_FILE, "r")
		file_as_str = f.read()
		# Split file content into lines
		lines = file_as_str.split('\n')
		# Edit first line
		lines[0] = "image: " + YAML_PATH + MAP_FILE
		# Close file
		f.close()
		# Open YAML_FILE for writing
		f = open(YAML_PATH + YAML_FILE, "w")
		# Add new-line character at the end of lines
		for i in range(len(lines)-1):
			lines[i] += '\n'
		# Rewrite YAML_FILE
		f.writelines(lines)
		# Close file
		f.close()
		# Success message
		rospy.loginfo("Success - Map file path in YAML_FILE edited!")


# Main function
def main(args):
	# Initialise node
	rospy.init_node('file_organizer')
	# Update POSE_PATH and MAP_FILE
	global POSE_FILE, MAP_FILE
	POSE_FILE += str(args[1]) + ".txt"
	MAP_FILE += str(args[1]) + ".pgm"
	# Instantise FileOrganizer
	fo = FileOrganizer()

			
# Check if the node is executing main
if __name__ == "__main__":
	main(sys.argv)

