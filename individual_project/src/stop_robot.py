#!/usr/bin/env python
'''
    *** THIS SCRIPT IS USED FOR DEBUGGING ONLY ***

	A simple script to stop the robot.

	author: Tomas Martinek, email: tomas-martinek@email.cz
'''

import rospy
from geometry_msgs.msg import Twist

def publisher():
	# Initialise node
	rospy.init_node('Walker', anonymous=True)
	# Create publisher
	pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
	# Set publish rate
	rate = rospy.Rate(10) #10hz
	# Initialise Twist message 'dv' as 'desired velocity'
	dv = Twist() # desired velocity
	dv.linear.x = 0
	dv.angular.z = 0
	# Publish 'dv' 10 times 
	for i in range(10):
		pub.publish(dv)
		rate.sleep()
		

if __name__ == "__main__":
	try:
		publisher()
	except rospy.ROSInterruptException:
		pass

