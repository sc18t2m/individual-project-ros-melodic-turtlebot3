#!/usr/bin/env python
'''
    Script remmembers and recognize a human actor from color
    of its T-shirt and publishes coordinates of its center point
    into 'center_point' topic to navigate the robot.

	Script subscribes to topics:
      * 'darknet_ros/bounding_boxes' to get coordinates of bounding
        boxes around actors from 'camera/rgb/image_raw' topic.
      * 'camera/rgb/image_raw' to get camera image in order to classify
        pixel colors.
      * 'gui_qt' to get instruction from user controlled GUI.

    Script publishes into topics:
	  * 'center_point' to publish coordinates of human actor to follow.
	  * 'initial_pose' to set initial position in RViz map.
	  * 'move_base/goal' to navigate robot using RViz map.

	author: Tomas Martinek, email: tomas-martinek@email.cz
'''

from __future__ import division
import rospy
import sys
import cv2
import numpy as np                                      # To convert numpy.uint8 to int
from sensor_msgs.msg import Image                       # To get distances of a point
from std_msgs.msg import Int16, Int16MultiArray, String # Types for subscribed topics
from cv_bridge import CvBridge, CvBridgeError           # OpenCV class
from darknet_ros_msgs.msg import BoundingBoxes          # To get center human coordinates

SCREEN_HEIGHT = 480
SENSITIVITY = 1 # 100 is working pretty much the same as center_point.py


class ActorRecognizer():
    def __init__(self):
        # Initialise publisher to center point of human to follow
        self.pub = rospy.Publisher('center_point', Int16MultiArray, queue_size=1)
        # Initialise CvBridge
        self.bridge = CvBridge()
        # Initialise T-shirt color of human to follow
        self.human_color = -1
        # bool_follow True -> publish center_point; bool_new_human True -> store new color
        self.bool_new_human = False
        self.bool_follow = False
        # Center point coordinates
        self.center_x = 0
        self.center_y = 0
        # Initialise list of points within bounding boxes that need to be checked
        # in order to recognize human
        self.point_array = []
        # Subscribe to human detection topic, RGB camera and 'gui_qt' GUI
        rospy.Subscriber('darknet_ros/bounding_boxes', BoundingBoxes, self.bounding_boxes)
        rospy.Subscriber('camera/rgb/image_raw', Image, self.get_color)
        rospy.Subscriber('gui_qt', String, self.set_command)
        # Previous 'gui_qt' instruction
        self.previous_gui_qt = ''

    def set_command(self, data):
        # Forget human if the instruction was changed from 'follow' to 'reset'
        if self.previous_gui_qt == "follow" and data.data == "reset":
            self.human_color = -1
            rospy.loginfo("Just forgot a human.")
        # Remmember new human if the instruction was changed from 'reset' to 'follow'
        if (self.previous_gui_qt == "reset" or self.previous_gui_qt == '') and data.data == "follow":
            self.bool_new_human = True
            # Reset center values
            self.center_x = 0
            self.center_y = 0
            rospy.loginfo("Remmember new human now!")
        # Set command; follow -> publish center_point, reset -> do nothing
        if data.data == "follow":
            self.bool_follow = True
            rospy.loginfo("Following activated!")
        if data.data == "reset":
            self.bool_follow = False
            rospy.loginfo("Following disabled.")
        # Set previous_gui_qt
        self.previous_gui_qt = data.data

    def bounding_boxes(self, data):
        # If new human is to be recognized
        if self.bool_new_human:
            # Maximum width of bounding box - likely the closest human
            max_width = 150 # initial value
            # Loop all bounding boxes
            for box in data.bounding_boxes:
                # Calculate box width
                width = box.xmax - box.xmin
                # Compare with max_width
                if width > max_width:
                    max_width = width
                    # Get center point of the box
                    provisional_center_x = (box.xmax + box.xmin) / 2
                    provisional_center_y = (box.ymax + box.ymin) / 2
            # If box was found, assign its center coordinates
            if max_width != 150:
                self.center_x = provisional_center_x
                self.center_y = provisional_center_y
            # Terminal info
            rospy.loginfo("New human center point assigned!")

        # If human is already recognized
        else:
            # Clear point_array (equivalent of point_array.clear() in Python 3)
            del self.point_array[:]
            # Loop all bounding boxes to create point_array to recognize actor;
            # for each bounding box ten pixels are checked.
            for index, box in enumerate(data.bounding_boxes):
                center_x = (box.xmax + box.xmin) / 2
                center_y = (box.ymax + box.ymin) / 2
                box_width = box.xmax - box.xmin
                box_height = box.ymax - box.ymin
                self.point_array.append([
                    [center_x, center_y],
                    [center_x, center_y + box_height * 0.15],
                    [center_x, center_y + box_height * 0.30],
                    [center_x, center_y - box_height * 0.15],
                    [center_x, center_y - box_height * 0.30],
                    [center_x + box_width * 0.15, center_y],
                    [center_x + box_width * 0.15, center_y + box_height * 0.15],
                    [center_x + box_width * 0.15, center_y - box_height * 0.15],
                    [center_x - box_width * 0.15, center_y],
                    [center_x - box_width * 0.15, center_y + box_height * 0.15],
                    [center_x - box_width * 0.15, center_y - box_height * 0.15] ])
            #rospy.loginfo("point_array for bounding_boxes assigned")
            #rospy.loginfo(self.point_array)

    def get_color(self, data):
        # Return if following is disabled
        if not self.bool_follow:
            rospy.loginfo("Not following, get_color function -> return")
            return

        # Get 2D matrix of HSV values of input image
        try:
            # Convert sensor_msgs/Image into opencv image (RGB)
            cv_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
        except:
            rospy.loginfo("Error OpenCV - while converting sensor_msgs/Image to OpenCV image!")
        # Convert RGB image into HSV image
        hsv_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)

        # If center point is not set, return
        if self.center_x == 0 or self.center_y == 0:
            return

        # Remmember color of a new human to follow
        if self.bool_new_human:
            # Get Y coordinate of a pixel down from center of the bounding box
            # around the closest human to remmember color of - almost certainly a T-shirt.
            cord_y = self.center_y + (SCREEN_HEIGHT - self.center_y) / 1.5
            # Get hue (HSV) value of a chosen pixel
            pixel_hue = hsv_image[int(cord_y), int(self.center_x), 0]
            # Print the hue value in terminal
            rospy.loginfo(hsv_image[int(self.center_y), int(self.center_x)])
            # Set new color of actor to be followed
            self.human_color = pixel_hue
            # Set to False because the actor was already assigned
            self.bool_new_human = False
            # Terminal info
            rospy.loginfo("New human assigned, HUE = " + str(self.human_color))

        # Check colors of all bounding_boxes around humans
        else:
            # At least 3 out of 10 must be matched in order to classify the actor
            max_points_matched = 3 # initialisation value
            # Bounding box index
            index = -1 # initialisation value
            # For every points of every bouding box
            for i, points in enumerate(self.point_array):
                # Counter of matched pixels
                counter = 0 # initialisation value
                # For every point regarding one bounding box
                for point in points:
                    # Get hue (HSV) value of the point
                    pixel_hue = hsv_image[int(point[1]), int(point[0]), 0]
                    # If hue is within range, increment counter
                    if pixel_hue + SENSITIVITY > self.human_color > pixel_hue - SENSITIVITY:
                        counter += 1
                # Update max_point_matched if a box contains enough matching pixels
                if counter > max_points_matched:
                    max_points_matched = counter
                    # Update bounding box index
                    index = i
                    # Terminal info
                    rospy.loginfo("Counter of matched pixels: " + str(counter))
            # If index was changed from initialisation value
            if index != -1:
                # Use center point of the bounding box to be published (center point is always 0th
                # point in the array)
                center_point = Int16MultiArray( data=[self.point_array[index][0][0], self.point_array[index][0][1]] )
                # Publish center point
                self.pub.publish(center_point)
            # Terminal info
            rospy.loginfo("***** Point_array checked! *****")


# Main function
def main(args):
    # Initialise node
    rospy.init_node('actor_recognizer')
    # Instantise ActorRecognizer
    ar = ActorRecognizer()
    # Esnsures that node continues running
    try:
        rospy.spin()
    except KeyboardInterrupt:
        rospy.loginfo("Rospy spin error - keyboard interrupt!")

			
# Check if the node is executing main
if __name__ == "__main__":
	main(sys.argv)