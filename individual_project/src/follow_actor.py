#!/usr/bin/env python
'''
	Script moves robot to follow a human actor or to return into its initial
	position that is setted also setted by this script.

	Script subscribes to topics:
	  * 'center_point' and 'pixel_depth' to receive information about
		position of a point to follow.
	  * 'gui_qt' to get information from user input into GUI.
	  * 'initial_pose' to ensure intial position on RViz map was set properly.

    Script publishes into topics:
	  * 'cmd_vel' to move the robot.
	  * 'initial_pose' to set initial position in RViz map.
	  * 'move_base/goal' to navigate robot using RViz map.

	author: Tomas Martinek, email: tomas-martinek@email.cz
'''

import rospy
import sys
from std_msgs.msg import Float32, Int16MultiArray, String, Float32MultiArray	# For topic subscriptions
from geometry_msgs.msg import Twist, PoseWithCovarianceStamped	# For moving the robot and the initial position
from move_base_msgs.msg import MoveBaseActionGoal				# For moving the robot with coordinates

SPEED_CONSTANT = 0.6 # higher the constant, higher the speed of robot
ROTATION_CONSTANT = 0.003 # higher the constant, faster the rotation of robot
SCREEN_WIDTH = 640
INITIAL_X = 0
INITIAL_Y = 0
INITIAL_W = 0


class ActorFollower():
	def __init__(self):
		# Publish to cmd_wel topic - controls robot moevemnt
		self.pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
		# Bool to determine whether to follow an actor
		self.bool_follow = False
		# Initialise center (x coordinate only), distance and desired velocity (dv)
		self.center = 0
		self.distance = 0.0
		self.dv = Twist()
		# Subscribe to 'center_point' and 'pixel_depth' to get approxmation of person location
		rospy.Subscriber('file_organizer', Float32MultiArray, self.set_initial_cords)
		rospy.Subscriber('center_point', Int16MultiArray, self.set_center)
		rospy.Subscriber('pixel_depth', Float32, self.set_distance)
		rospy.Subscriber('gui_qt', String, self.set_command)
		rospy.Subscriber('initialpose', PoseWithCovarianceStamped, self.set_bool_initial_pose)
		# Was initial position set? (the counter explained in set_bool_initial_pose)
		self.bool_initial_pose = False
		self.initial_pose_counter = 0
		# Previous 'gui_qt' instruction - changed to 'reset' -> stop moving
		self.previous_gui_qt = ''

	def set_initial_cords(self, data):
		global INITIAL_X, INITIAL_Y, INITIAL_W
		INITIAL_X, INITIAL_Y, INITIAL_W = data.data

	def set_center(self, data):
		# Set center
		self.center = data.data[0]

	def set_distance(self, data):
		# Set distance
		if (data.data > 0.1 and data.data < 5.0):
			self.distance = data.data
		self.move_robot()

	def set_command(self, data):
		# Stop moving if the instruction was changed from 'follow' to 'reset'
		if self.previous_gui_qt == "follow" and data.data == "reset":
			self.stop_moving()
		# Set command; follow -> follow actor, reset -> go back to initial position
		if data.data == "follow":
			self.bool_follow = True
		if data.data == "reset":
			self.bool_follow = False
			self.return_to_initial_position()
		# Set previous_gui_qt
		self.previous_gui_qt = data.data

	def set_bool_initial_pose(self, data):
		# For some reason, 'initialpose' must be published multiple times for RViz to register.
		# This seems to be a bug, proposed solution is not ideal, but works like a charm.
		self.initial_pose_counter += 1
		if self.initial_pose_counter == 5:
			# Initial pose was set, do not set it again
			self.bool_initial_pose = True

	def set_initial_pose(self):
		# Publish to 'initialpose' topic - controls robot initial position in RViz map
		self.initial_pub = rospy.Publisher('initialpose', PoseWithCovarianceStamped, queue_size=1)
		# Create 'PoseWithCovarianceStamped' variable
		initial_pose = PoseWithCovarianceStamped()
		initial_pose.pose.pose.position.x = INITIAL_X
		initial_pose.pose.pose.position.y = INITIAL_Y
		initial_pose.pose.pose.orientation.w = INITIAL_W
		# Publish initial_pose
		self.initial_pub.publish(initial_pose)

	def return_to_initial_position(self):
		# Publish to 'move_base/goal' topic - controls robot movement with RViz map
		self.return_pub = rospy.Publisher('move_base/goal', MoveBaseActionGoal, queue_size=1)
		# Create 'MoveBaseActionGoal' variable
		goal_pose = MoveBaseActionGoal()
		goal_pose.goal.target_pose.header.frame_id = "map"
		goal_pose.goal.target_pose.pose.position.x = INITIAL_X
		goal_pose.goal.target_pose.pose.position.y = INITIAL_Y
		goal_pose.goal.target_pose.pose.orientation.w = INITIAL_W
		# Publish goal_pose
		self.return_pub.publish(goal_pose)

	def stop_moving(self):
		# Initialise 'stop_velocity'
		stop_velocity = Twist() # by default all variables are 0.0
		# Publish desired velocity to cmd_vel to move robot
		self.pub.publish(stop_velocity)

	def set_speed(self): # Not used if speed is set dynamically
		# Set forward speed accordingly to distance
		if (self.distance > 4.0):
			speed = 1.0
		elif (self.distance > 3.0):
			speed = 0.8
		elif (self.distance > 2.0):
			speed = 0.7
		else:
			speed = 0.5
		# Lower speed in case the robot is turning
		if (self.dv.angular.z != 0):
			speed = 0.3
		# Set the speed
		self.dv.linear.x = speed

	def move_robot(self):
		# Check if following is enabled
		if not self.bool_follow:
			return

		# Set initial position
		if not self.bool_initial_pose:
			self.set_initial_pose()
		
		# ***** TURNING *****
		# Dynamically set speed
		self.dv.angular.z = (SCREEN_WIDTH/2 - self.center) * ROTATION_CONSTANT
		'''
		# Statically set speed
		# Right turn
		if (self.center > SCREEN_WIDTH/2+20):
			message = "Turning RIGHT, "
			self.dv.angular.z = -0.3
		# Left turn
		elif (self.center < SCREEN_WIDTH/2-20):
			message = "Turning LEFT,  "
			self.dv.angular.z = 0.3
		# Stop turn
		else:
			message = "Turning STOP,  "
			self.dv.angular.z = 0.0
		'''

		# ***** MOVING *****
		# Dynamically set rotation speed
		self.dv.linear.x = (self.distance-1) * SPEED_CONSTANT
		'''
		# Statically set rotation speed
		# Move backwards
		if (self.distance < 0.8):
			message += "Moving BACKWARDS"
			self.dv.linear.x = -0.2
		# Move forward
		elif (self.distance > 1.2):
			self.set_speed() # Forward speed depends on many factors
			message += "Moving FORWARD, Speed " + str(self.dv.linear.x)
		# Stop moving
		else:
			message += "Moving STOP"
			self.dv.linear.x = 0.0
		'''

		# Publish desired velocity to cmd_vel to move robot
		self.pub.publish(self.dv)
		# Terminal log - print 'meesage' string with movement info
		rospy.loginfo(message)
			

# Main function
def main(args):
	# Initialise node
	rospy.init_node('follow_actor')
	# Instantise ActorFollower
	af = ActorFollower()
	# Esnsures that node continues running
	try:
		rospy.spin()
	except KeyboardInterrupt:
		rospy.loginfo("Rospy spin error - keyboard interrupt!")

			
# Check if the node is executing main
if __name__ == "__main__":
	main(sys.argv)

