#!/usr/bin/env python
'''
    *** THIS SCRIPT IS USED FOR DEBUGGING ONLY ***

	Script subscribes to topic 'darknet_ros/bounding_boxes' to obtain
	coordinates of human actor and turns robot to keep the actor in center
	of the camera view by publishing to 'cmd_vel' topic.

	author: Tomas Martinek, email: tomas-martinek@email.cz
'''

import rospy
from geometry_msgs.msg import Twist
from darknet_ros_msgs.msg import BoundingBoxes

def publisher():
	# Initialise node
	rospy.init_node('Walker', anonymous=True)
	# Subscribe to 'darknet_ros/bounding_boxes'
	rospy.Subscriber('darknet_ros/bounding_boxes', BoundingBoxes, turn)
	# Keep node running
	rospy.spin()

def turn(data):
	# Loop through all bounding boxes
	for bounding_box in data.bounding_boxes:
		# Get bounding box center
		bounding_box_center = (bounding_box.xmax + bounding_box.xmin) / 2
		# Print box center and its class
		rospy.loginfo("Class: " + bb.Class + "\tCenter:" + str(bounding_box_center))
		# Return if bounding box doesn't surround a 'person'
		if (bounding_box.Class != "person"):
			return
		# Create publisher for 'cmd_vel' topic
		pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
		rate = rospy.Rate(10) #10hz
		dv = Twist() # desired velocity
		# Turn right if the actor is on the left side
		if (bounding_box_center > 340):
			rospy.loginfo("Turning RIGHT")
			dv.angular.z = -0.3
		# Turn left if the actor is on the right side
		elif (bounding_box_center < 270):
			rospy.loginfo("Turning LEFT")
			dv.angular.z = 0.3
		# Stop turning if the actor is withing a range in the middle
		else:
			rospy.loginfo("Turning STOP")
			dv.angular.z = 0

		# Publish desired velocity 'dv'
		pub.publish(dv)
			
		
if __name__ == "__main__":
	try:
		publisher()
	except rospy.ROSInterruptException:
		pass

