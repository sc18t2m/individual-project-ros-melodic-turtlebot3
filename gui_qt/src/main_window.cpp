/**
 * @file /src/main_window.cpp
 *
 * @brief Implementation for the qt gui.
 *
 * @date February 2011
 **/
/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui>
#include <QMessageBox>
#include <iostream>
#include "../include/gui_qt/main_window.hpp"

/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace gui_qt {

using namespace Qt;

/*****************************************************************************
** Implementation [MainWindow]
*****************************************************************************/

MainWindow::MainWindow(int argc, char** argv, QWidget *parent)
	: QMainWindow(parent)
	, qnode(argc,argv)
{
	// Calling this incidentally connects all ui's triggers to on_...() callbacks in this class.
	ui.setupUi(this);

	// Close GUI after ctrl+C in terminal
    QObject::connect(&qnode, SIGNAL(rosShutdown()), this, SLOT(close()));

    // If there is not roscore running
	if ( !qnode.init() ) {
		// Error message
		showNoMasterMessage();
		// Disable buttons
		ui.push_follow->setEnabled(false);
		ui.push_return->setEnabled(false);
	}
}

MainWindow::~MainWindow() {}

// Show message window and disable buttons if roscore is not running
void MainWindow::showNoMasterMessage() {
	QMessageBox msgBox;
	msgBox.setText("Couldn't find the ros master.");
	msgBox.exec();
    close();
}

// QPushButton push_follow on click event
void MainWindow::on_push_follow_clicked( bool check )
{
	// Change buttons color
	ui.push_follow->setStyleSheet("QPushButton { background-color: lightgreen; }");
	ui.push_return->setStyleSheet("QPushButton { background-color: white; }");

	// Change message to be published into 'gui_qt' topic
	qnode.gui_message = "follow";
}

// QPushButton push_return on click event
void MainWindow::on_push_return_clicked( bool check )
{
	// Change buttons color
	ui.push_return->setStyleSheet("QPushButton { background-color: lightgreen; }");
	ui.push_follow->setStyleSheet("QPushButton { background-color: white; }");

	// Change message to be published into 'gui_qt' topic
	qnode.gui_message = "reset";
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	QMainWindow::closeEvent(event);
}

}  // namespace gui_qt

