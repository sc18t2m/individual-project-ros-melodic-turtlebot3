/**
 * @file /include/gui_qt/main_window.hpp
 *
 * @brief Qt based gui for gui_qt.
 *
 * @date November 2010
 **/
#ifndef gui_qt_MAIN_WINDOW_H
#define gui_qt_MAIN_WINDOW_H

/*****************************************************************************
** Includes
*****************************************************************************/

#include <QtGui/QMainWindow>
#include "ui_main_window.h"
#include "qnode.hpp"

/*****************************************************************************
** Namespace
*****************************************************************************/

namespace gui_qt {

/*****************************************************************************
** Interface [MainWindow]
*****************************************************************************/
/**
 * @brief Qt central, all operations relating to the view part here.
 */
class MainWindow : public QMainWindow {
Q_OBJECT

public:
	MainWindow(int argc, char** argv, QWidget *parent = 0);
	~MainWindow();

	void closeEvent(QCloseEvent *event); // Overloaded function
	void showNoMasterMessage();

public Q_SLOTS:
	/******************************************
	** Auto-connections (connectSlotsByName())
	*******************************************/
	void on_push_follow_clicked( bool check );
	void on_push_return_clicked( bool check );

private:
	Ui::MainWindowDesign ui;
	QNode qnode;
};

}  // namespace gui_qt

#endif // gui_qt_MAIN_WINDOW_H
